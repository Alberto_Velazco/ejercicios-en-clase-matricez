﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matricez
{
    class ejercicio
    {
        /// <summary>
        /// Programa que tiene como concepto el nombre de 4 empleados, ingreso
        /// en conceptos de sueldo cobrado por cada emepleado, en los ultimos 3 meses.
        /// Por medio de una matriz organizaremos los datos anteriores, dentro de la matriz
        /// se pediran los datos para despues por medio de un if poder separar los
        /// resultados que se piden mostrar.
        /// </summary>
        public static void programa1()
        {
             int total = 0;
            Console.WriteLine("Se necesita obtener la informacion de 4 empleados de la empresa:");
            string[] empleados = new string [4];
            int[,] sueldos = new int[4, 3];
            for (int i=1; i < 5; i++)
            {
                Console.WriteLine("\n" +
                    "Anote el nombre del empleado "+i);
                string nombre;
                nombre = Console.ReadLine();
                for (int j = 0; j < 3; j++)
                {
                    Console.WriteLine("Anote el sueldo del empleado");
                    string pago;
                    pago = Console.ReadLine();
                    sueldos[i, j] = int.Parse(pago);
                    total = total + sueldos[i, j];
                }
                System.Console.Clear();
            }
            Console.WriteLine("El total pagado estos ultimos 3 meses es de: $" + total);
            Console.ReadKey();
            System.Console.Clear();
        }
        public static void programa2()
        {
     int N = 0; int MI = 0; string linea;
            Console.Write("\n" +
                "Anote el tamano de la matiz:\n" +
                "");
            linea = Console.ReadLine();
            N = int.Parse(linea);
            N = (N % 2 == 0 ? N + 1 : N);
            string[,] MAT = new string[N + 1, N + 1];
            System.Console.Clear();
            for (int a = 1; a <= N; a++)
            {
                for (int b = 1; b <= N; b++)
                {
                    MAT[a, b] = " ";
                }
            }
            MI = N / 2 + 1;
            for (int a = 1; a <= N; a++)
            {
                MAT[a, 1] = "R";
                MAT[a, N] = "R";
                MAT[MI, a] = "A";
                MAT[1, a] = "A";
            }
            for (int a = 1; a <= N; a++)
            {
                for (int b = 1; b <= N; b++)
                {
                    Console.SetCursorPosition(b, a + 1);
                    Console.Write(MAT[a, b]);
                }
            }
            Console.WriteLine("\n" +
                "Presione una tecla");
            Console.ReadKey();
            System.Console.Clear();
        }
        public static void programa3()
        {

        }
        public static void programa4()
        {

        }
        public static void programa5()
        {

        }
        public static void programa6()
        {

        }
    }
}
