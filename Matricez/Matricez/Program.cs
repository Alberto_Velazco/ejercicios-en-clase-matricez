﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matricez
{
    class Program
    {
        /// <summary>
        /// Menu que nos dara las opciones diponibles (jercicos).
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            /// (a) la utilizaremos como condicional para que se repita el ciclo
            int a = 0;
            do
            {
                Console.WriteLine("Menú de los ejercicios en clase");
                Console.WriteLine("Las opciones disponibles son: \n" +
                "1)Sueldo de empleados\n" +
                "2)Formar una letra R en una matrix NxN \n" +
                "3)Numero mayor y menor en una matriz NxN \n" +
                "4)Ordenamiento de una matrix NxN\n" +
                "5)Menores de cada columna de una matriz NxN en un vector\n" +
                "6)Mayores de cada fila de una matriz NxN en un vector\n" +
                "7)Para salir del menú y cerrar el programa.");
                Console.WriteLine("\n" +
                "Anota la opción que deseas");
                /// (A) va a guardar la opción elegida del menú principal
                string A = String.Empty;
                A = Console.ReadLine();
                switch (A)
                {
                    case "1":
                        ejercicio.programa1();
                        a = a + 1;
                        break;
                    case "2":
                        ejercicio.programa2();
                        a = a + 1;
                        break;
                    case "3":
                        ejercicio.programa3();
                        a = a + 1;
                        break;
                    case "4":
                        ejercicio.programa4();
                        a = a + 1;
                        break;
                    case "5":
                        ejercicio.programa5();
                        a = a + 1;
                        break;
                    case "6":
                        ejercicio.programa6();
                        a = a + 1;
                        break;
                    case "7":
                        a = a - 2;
                        break;
                    default:
                        Console.WriteLine("La opción que elegiste no existe");
                        Console.WriteLine("Favor de intentar nuevamente");
                        a = a + 1;
                        break;
                }
                System.Console.Clear();
            } while (a >= 1);
            Console.WriteLine("\n" +
            "El programa ha sido cerrado correctamente");
            Console.ReadKey();
        }
    }
}
